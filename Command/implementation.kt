interface Command {
    fun execute()
}

class AddCommand(val vegetable: String) : Command {
    override fun execute() = println("adding $vegetable")
}

class Container {
    private val list = ArrayList<Command>()

    fun add(command: Command): Container = apply {
        list.add(command)
    }
    fun process(): Container = apply {
        list.forEach {it.execute()}
    }
}
fun main() {
   Container()
        .add(AddCommand("tomato"))
        .add(AddCommand("cucumber"))
        .add(AddCommand("Potato"))
        .process()
}