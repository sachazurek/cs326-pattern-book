// Interface creates a generic Shape
interface Shape {
    fun area(): Int
}

// Rectangle() defines a specifc shape
class Rectangle(val height: Int, val width: Int): Shape {
    override fun area() = width * height
}

/*
Window is composed of objects that implement Shape,
and functionality is delegated to its composite objects.

This makes use of Kotlin's built in support of delegation,
through the "by" keyword.
 */
class Window(private val bounds: Shape) : Shape by bounds

fun main() {
    val window = Window(Rectangle(2,2))
    println(window.area())
}