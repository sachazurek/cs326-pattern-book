
// In Kotlin, object keyword creates a class that is lazy-initialized on first access
// In Java, the code would look like this:
/*
public final class SomeSingleton {
   public static final SomeSingleton INSTANCE;

   private SomeSingleton() {
      INSTANCE = (SomeSingleton)this;
   }

   static {
      new SomeSingleton();
   }
}
*/
// Plus whatever user-defined functions.
// Information gathered from: https://medium.com/@BladeCoder/kotlin-singletons-with-argument-194ef06edd9e
// with respect.
object SingletonExample {
    fun plusOne(input: Int) : Int {
        return (input + 1)
    }
}

fun main() {
    println("Testing singleton, enter an integer: ")
    var input : Int = Integer.valueOf(readLine())
    println(SingletonExample.plusOne(input))

}

/*
    In this implementation, the Singleton represents some kind of object that recieves input
    and reacts to it. Only one instance of the object is required, and multiple instances could
    create scoping errors and inconsistent behavior in larger applications. 
*/