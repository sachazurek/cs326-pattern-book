interface GenericInteger{
    abstract fun getVal() 
}

class MaxByte() : GenericInteger {
    override fun getVal(){
        println(127)
    }
}

class MaxShort() : GenericInteger {
    override fun getVal() {
        println(32767)
    }
}

enum class SupportedInts {
    Byte,Short
}

object Factory {
    fun GetMaxInt(type : SupportedInts) : GenericInteger{
        when (type) {
            SupportedInts.Byte -> return MaxByte()
            SupportedInts.Short -> return MaxShort()
            else -> throw IllegalArgumentException()
        }
    }
}

fun main() {
    Factory.GetMaxInt(SupportedInts.Byte).getVal()
    Factory.GetMaxInt(SupportedInts.Short).getVal()
}