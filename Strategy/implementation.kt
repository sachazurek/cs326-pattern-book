interface abstractMethod {
    fun doWork();
}

class methodTypeOne() : abstractMethod {
    override fun doWork() {
        println("Type 1 Behavior")
    }
}

class methodTypeTwo() : abstractMethod {
    override fun doWork() {
        println("Type 2 Behavior")
    }
}

class Program(var method : abstractMethod) {
    fun process() {
        method.doWork()
    }
}

fun main() {
    var program : Program = Program(methodTypeOne())
    program.process()
    program = Program(methodTypeTwo())
    program.process()
}