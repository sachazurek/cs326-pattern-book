object Originator {
    private var state = String()

    fun setState(id : String) {
        state = id
    }

    fun storeState() {
        mementoHolder.add(Memento(this.state))
    }
    fun restoreState(memento : Memento) {
        setState(memento.getState())
    }
    fun printState() {
        println(state)
    }
}

class Memento(key : String) {
    private val key: String = key

    fun getState() : String {
        return key
    }
}

object mementoHolder {
    private val list = ArrayList<Memento>()

    fun add(memento : Memento) {
        list.add(memento)
    }
    fun getNthState(int : Int) : Memento {
        // unsafe, just representative)
        return list.get(int)
    }
    fun toDisk() {
        /* Many memento use cases would involve
        being able to save the states to disk
        between runs of the application */
    }
    fun loadFromDisk() {
        /* See above */
    }
}


fun main() {
    Originator.setState("Half-capacity")
    Originator.printState()
    Originator.setState("Full Capacity")
    Originator.printState()
    Originator.storeState()
    Originator.setState("Oops, broken")
    Originator.printState()
    Originator.restoreState(mementoHolder.getNthState(0))
    Originator.printState()
}